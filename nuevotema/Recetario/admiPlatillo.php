<?php  
use leviathan\modelos\Platillos;
include "../../leviathan/modelos/Conexion.php";
include "../../leviathan/modelos/Platillos.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>AdmiPlatilos</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">


  </head>
  <body class="goto-here">
    <div class="py-1 bg-primary">
      <div class="container">
        <div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
          <div class="col-lg-12 d-block">
            <div class="row d-flex">
              <div class="col-md pr-4 d-flex topper align-items-center">
                <div class="icon mr-2 d-flex justify-content-center align-items-center"></div>
                
              </div>
              <div class="col-md pr-4 d-flex topper align-items-center">
                <div class="icon mr-2 d-flex justify-content-center align-items-center"></div>
              </div>
             
            </div>
          </div>
        </div>
      </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
      <div class="container">
        <a class="navbar-brand" href="index.html">AdmiPlatillos</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Menu
        </button>
       <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active"><a href="indexAdmi.html" class="nav-link">Inicio</a></li>
            <li class="nav-item dropdown">
            </li>
            <li class="nav-item"><a href="http://localhost/nuevotema/Recetario/admiPlatillos.html" class="nav-link">Platillos</a></li>
            <li class="nav-item"><a href="http://localhost/nuevotema/Recetario/admiPostres.html" class="nav-link">Postres</a></li>
            <li class="nav-item"><a href="http://localhost/nuevotema/Recetario/admiBebidas.html" class="nav-link">Bebidas</a></li>
           
          </ul>
        </div>
    </nav>
    <!-- END nav -->
     <?php  
        $buscar=$_POST['id'];
    ?>


 
</div align="center">
            <a href="../RegistroPlatillos/registro_platillo.html" type="submit" value="Registrar" style="background: blue; border-radius: 3px; text-decoration: none; text-align: center; color: white; font-size: 20px; width: 200px; margin-left: 8%" >Registrar datos</a>
          </div>
    <section class="ftco-section ftco-cart">
      <div class="container">
        <div class="row">
          <div class="col-md-12 ftco-animate">
            <div class="cart-list">
              <table class="table">
                <thead class="thead-primary">
                  <tr class="text-center">
                    <th>Id_platillo</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Tipo</th>
                    <th>Fecha publicada </th>
                    <th>Actualizar </th>
                    <th>Eliminar </th>

                  </tr>
                </thead>
                <tbody id="tbody">
                  <?php  
                  $ntabla=Platillos::busca($buscar);
                  foreach ($ntabla as $key) {
                  ?>
                  <tr>
                    <td><?php echo $key['Id_Platillos']; ?></td>
                    <td><?php echo $key['Nombre']; ?></td>
                    <td><?php echo $key['Descripcion']; ?></td>
                    <td><?php echo $key['Tipo']; ?></td>
                    <td><?php echo $key['FechaPublicada']; ?></td>
                    <td><button id="cambiar"  data-target="#ModalHelp" data-toggle="modal" data-id='<?php echo $key['Id_Platillos']; ?>' data-nombre='<?php echo $key['Nombre']; ?>' data-descripcion='<?php echo $key['Descripcion']; ?>' data-tipo='<?php echo $key['Tipo']; ?>' data-fechapublicada='<?php echo $key['FechaPublicada']; ?>'>Actualizar</button></td>
                    <td><form action="/leviathan/index.php?controller=Platillos&action=borrar" method="POST">
                      <input name="id" id="id" type="hidden" value="<?php echo $key['Id_Platillos']; ?>">
                      <input type="submit" id="de acuerdo" value="Eliminar">
                      
                    </form></td> 
                  </tr>
                  
                  <?php  
 
                  }
                  ?>
                              
                </tbody>
              </table>
            </div>
          </div>
        </div>
        
    <section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
      <div class="container py-4">
        <div class="row d-flex justify-content-center py-5">
      
          <div class="col-md-6 d-flex align-items-center">
            <form action="#" class="subscribe-form">
              <div class="form-group d-flex">
                <input type="text" value="<?php echo("$buscar");?>" class="form-control" placeholder="Buscar por id">
                <input type="submit" id="busca"value="Buscar" class="submit px-3">
                <input type="submit"  id="imprimir" value="Mostrar datos" class="submit px-3">
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>


 <div class="modal fade" tabindex="-1" role="dialog" id="ModalHelp">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center all-tittles">ACTUALIZACIÓN</h4>
                </div>
                <div class="modal-body">
                    <form  action="/leviathan/index.php?controller=Platillos&action=cambiar"id="Res" method="POST" class="login100-form validate-form">
          <span class="login100-form-title p-b-49">
            Ingresar platillo
          </span>
          <div class="wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
            <span class="label-input100">id del platillo</span>
            <input class="input100" type="text" id="id_" name="id_" placeholder="id de platillo">
            <span  data-symbol="&#xf206;"></span>
          </div>
                    <!--Nombre del nuevo usuario-->
          <div class="wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
            <span class="label-input100">Nombre</span>
            <input class="input100" type="text" id="Nombre" name="Nombre" placeholder="Nombre">
            <span  data-symbol="&#xf206;"></span>
          </div>
                    <!--Apellido paterno del usuario-->
                    <div class="wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
            <span class="label-input100">Descripcion</span>
            <input class="input100" type="text" id="Descripcion" name="Descripcion" placeholder="Descripcion">
            <span  data-symbol="&#xf206;"></span>
          </div>
          <!--Apellido materno del usuario-->
          <div class="wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
            <span class="label-input100">Tipo</span>
            <input class="input100" type="text" id="Tipo" name="Tipo" placeholder="Tipo">
            <span  data-symbol="&#xf206;"></span>
          </div>
                    <!--Password o contraseña del usuario-->
          <div class="wrap-input100 validate-input" data-validate="Password is required">
            <span class="label-input100">Fecha Publicada</span>
            <input class="input100" type="text" id="FechaPublicada" name="FechaPublicada" placeholder="FechaPublicada">
            <span  data-symbol="&#xf190;"></span>
          </div>

          <div class="modal-footer">
                    <!--Este boton c:--><input type="submit" id="actuar">
                </div>
          
        
                </div>
                </form>
            </div>
          </div>
        </div>

    <footer class="ftco-footer ftco-section">
      <div class="container">
        <div class="row">
          <div class="mouse">
            <a href="#" class="mouse-icon">
              <div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
            </a>
          </div>
        </div>    
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="js/main.js"></script>

  
  </body>

  <script src="jquery.min.js" type="text/javascript"></script>
   <script src="jquery-3.4.1.main.js"></script>
   <script type="application/javascript">
    $(document).ready(function(){

    //Mostrar datos en la tabla
      /*function mostrar(){
        $.ajax({

          url:"http://localhost/leviathan/index.php?controller=Platillos&action=Todo",
          success:function(e){
            var js= JSON.parse(e);//Transforma el json en texto
            var tabla;

            for (var i = 0; i <js.length; i++) {
              tabla+='<tr><td>'
              +js[i].Id_Platillos+'</td><td>'
              +js[i].Nombre+'</td><td>'
              +js[i].Descripcion+'</td><td>'
              +js[i].Tipo+'</td><td>'
              +js[i].FechaPublicada+'</td><td>'
              +'<button id="cambiar"  data-target="#ModalHelp" data-toggle="modal" data-id='+js[i].Id_Platillos+' data-nombre='+js[i].Nombre+' data-descripcion='+js[i].Descripcion+' data-tipo='+js[i].Tipo+' data-fechapublicada='+js[i].FechaPublicada+'>Actualizar</button>'+'</td><td>'
              +'<button class="delete" data-id='+js[i].Id_Platillos+'>Eliminar</button>'+'</td></tr>';
              
            }
            $("#tbody").html(tabla);
          }

        });
      }
      mostrar();
    //Mostrar datos en la tabla


    //Eliminar datos
    function eliminar(){
      $(document).on('click','.delete',function(e){
        e.preventDefault();
        var id= $(this).data("id"); //Obtiene el id del boton
        /*alert(id);
        return false;*/
        /*var opcion=confirm('¿Estas seguro de eliminar el dato?'); //ppregunta
        if (opcion==true) { //Si da click en aceptar
          $.ajax({
            type:"get",
            url:"http://localhost/leviathan/index.php?controller=Platillos&action=borrar&id="+id,
            cache: false,
            success: function(e){
              alert("El dato ha sido elimindado");//E elimina el dato
              mostrar();//Manda a llamar la funcion de mortrar otra vez la tabla
            }
          });
        }else{
          alert("Ocurrio un error");
        }
      });
    }
    eliminar();
    //Elimnar datos

    //Buscar
    function buscarPos(){
       $(document).on('click','#busca',function(e){
        e.preventDefault();
          var bus=$(".form-control").val();
          $.ajax({
            type:"get",
            url:"http://localhost/leviathan/index.php?controller=Platillos&action=find&id="+bus,
            data:bus,
            success:function(f){
              var js=JSON.parse(f);
              var tabla;
              tabla+='<tr><td>'
              +js.Id_Platillos+'</td><td>'
              +js.Nombre+'</td><td>'
              +js.Descripcion+'</td><td>'
              +js.Tipo+'</td><td>'
              +js.FechaPublicada+'</td><td>'
              +'<button id="cambiar"  data-target="#ModalHelp" data-toggle="modal" data-id='+js.Id_Platillos+' data-nombre='+js.Nombre+' data-descripcion='+js.Descripcion+' data-tipo='+js.Tipo+' data-fechapublicada='+js.FechaPublicada+'>Actualizar</button>'+'</td><td>'
              +'<button class="delete" data-id='+js.Id_Platillos+'>Eliminar</button>'+'</td></tr>';
              
            $("#tbody").html(tabla);
            }
            //Buscar
          });
      });

    }
      buscarPos();
      $('#imprimir').click(function(){
                mostrar();
            });*/

      function muestradatos(){

        $(document).on('click','#cambiar', function(){
                    var codi=$(this).data("id");
                    var nom=$(this).data("nombre");
                    var desc=$(this).data("descripcion");
                    var tipo=$(this).data("tipo");
                    var fecha=$(this).data("fechapublicada");

                    var c=$("#id_").val(codi);
                    var n=$("#Nombre").val(nom);
                    var d=$("#Descripcion").val(desc);
                    var t=$("#Tipo").val(tipo);
                    var f=$("#FechaPublicada").val(fecha);
        });

      }

      /*function actualiza(){
        $("#actuar").click(function(e){
          e.preventDefault();
          $.ajax({
              type:"post",
              url:"http://localhost/leviathan/index.php?controller=Platillos&action=cambiar",
              data: $('#Res').serialize(),
              dataType: false,
              success:function(pato){
                  if(pato==0){
                    alert("Ocurrio un error intente de nuevo");
                                
                  }else{
                   mortrar();
                    alert("El dato ha sido actualizado");
                    console.log(pato);
                }
             }
          });

        });
      }
      actualiza();*/
      muestradatos();
    });
   </script>
        
</html>