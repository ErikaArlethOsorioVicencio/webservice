<?php
use leviathan\modelos\Postres;
include "../../leviathan/modelos/Conexion.php";
include "../../leviathan/modelos/Postres.php";  

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Postres</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body class="goto-here">
		<div class="py-1 bg-primary">
    	<div class="container">
    		<div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
	    		<div class="col-lg-12 d-block">
		    		<div class="row d-flex">
		    			<div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon mr-2 d-flex justify-content-center align-items-center"></div>
					    </div>
					    <div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon mr-2 d-flex justify-content-center align-items-center"></div> 
					    </div>
				    </div>
			    </div>
		    </div>
		  </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="postres.php">Postres</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      
            <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active"><a href="index.php" class="nav-link">Inicio</a></li>
            <li class="nav-item dropdown"></li>
        <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
           <li class="nav-item"><a href="platillos.php" class="nav-link">Platillos</a></li>
            <li class="nav-item"><a href="postres.php" class="nav-link">Postres</a></li>
            <li class="nav-item"><a href="bebidas.php" class="nav-link">Bebidas</a></li>
          </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->

    <div class="hero-wrap hero-bread" style="background-image: url('images/postres.jpg');">
      
    </div>

    <section class="ftco-section ftco-cart">
			<div class="container">
				<div class="row">
    			<div class="col-md-12 ftco-animate">
    				<div class="cart-list">
	    				<table class="table">
						    <thead class="thead-primary">
						      <tr class="text-center">
						        <th>Id_postre</th>
						        <th>Nombre</th>
						        <th>Descripción</th>
						        <th>Tipo</th>
						        <th>Fecha publicada </th>

						      </tr>
						    </thead>
						    <tbody id="tbody">
                  <?php  
                  $ntabla=Postres::all();
                  foreach ($ntabla as $key) {
                  ?>
                  <tr>
                    <td><?php echo $key['Id_Postres']; ?></td>
                    <td><?php echo $key['Nombre']; ?></td>
                    <td><?php echo $key['Descripcion']; ?></td>
                    <td><?php echo $key['Tipo']; ?></td>
                    <td><?php echo $key['FechaPublicada']; ?></td>
            
                      
                    </form>
                  </tr>
                  
                  <?php  
 
                  }
                  ?>
												      
						    </tbody>
						  </table>
					  </div>
    			</div>
    		</div>
    		
    					

		<section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
      <div class="container py-4">
        <div class="row d-flex justify-content-center py-5">
          <div class="col-md-6">
          	
          </div>
          
    </section>
    <footer class="ftco-footer ftco-section">
      <div class="container">
      	<div class="row">
      		<div class="mouse">
						<a href="#" class="mouse-icon">
							<div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
						</a>
					</div>
      	</div>
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Postres</h2>
              <p><h2>Un postre es el complemneto dulce que alegra al paldar.</h2></p>
               <div class="ftco-footer-widget mb-2">
              <h2 class="ftco-heading-2">Acerca de nosotros</h2>
              <p><h2>Somos los encargados de dar información acerca de las diversas recetas que puedes crear satisfaciendo la necesidad de poder hacer consultas rapidas.</h2></p>
            </div>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Menu</h2>
              <ul class="list-unstyled">
                <li><a href="platillos.php" class="py-2 d-block"><h2>Platillos</h2></a></li>
                <li><a href="bebidas.php" class="py-2 d-block"><h2>Bebidas</h2></a></li>
                <li><a href="postres.php" class="py-2 d-block"><h2>Postres</h2></a></li>
              </ul>
            </div>
          </div>

          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Contactanos</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text"><h2>leviathaan20@outlook.com</h2></span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/scrollax.min.js"></script>
  <!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>-->
  <script src="js/main.js"></script>

    
  </body>
   <script src="jquery.min.js" type="text/javascript"></script>
   <script src="jquery-3.4.1.main.js"></script>
   <script type="application/javascript"></script>
</html>