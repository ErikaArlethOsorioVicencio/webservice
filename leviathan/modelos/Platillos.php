<?php  
namespace leviathan\modelos;
class Platillos extends Conexion
{
    public $Id_Platillos;
	public $Nombre;
	public $Descripcion;
	public $Tipo;
	public $FechaPublicada;

	function insertar(){
		
		$pre = mysqli_prepare($this->con, "INSERT INTO platillos(Nombre, Descripcion, Tipo, FechaPublicada) VALUES (?,?,?,?)");
		$pre->bind_param("ssss",$this->Nombre, $this->Descripcion, $this->Tipo, $this->FechaPublicada);		
		$pre->execute();
		
		header("location: ../nuevotema/RegistroPlatillos/registro_platillo.html ");
	
		
	}
	static function busca($id)
	{
		$me = new Conexion();
		$pre = mysqli_prepare($me->con, "SELECT * FROM platillos WHERE Id_Platillos= ?");
		$pre->bind_param("i",$id);
		$pre->execute();
		$res=$pre->get_result();
		while ($k=mysqli_fetch_assoc($res)) {
			$z[]=$k;
		}
		return $z;


	}

	static function buscar($nom){
		$me = new Conexion();
		$pre = mysqli_prepare($me->con, "SELECT * FROM platillos WHERE Nombre= ?");
		$pre->bind_param("s",$nom);
		$pre->execute();
		$res=$pre->get_result();
		return $res->fetch_object();

	}
	static function all(){
		$me = new Conexion();
		$pre = mysqli_prepare($me->con, "SELECT * FROM platillos");
		$pre->execute();
		$res=$pre->get_result();
		while ($a=mysqli_fetch_assoc($res)) {
			$h[]=$a;
			
		}
		
		return $h;
	}

	static function eliminar($quita){
		$me = new Conexion();
		$pre = mysqli_prepare($me->con, "DELETE FROM platillos WHERE Id_Platillos= ?");
		$pre->bind_param("i",$quita);
		$pre->execute();
		header("location: ../nuevotema/Recetario/admiPlatillos.php");


	}
	 function actualizar(){
		$pre_ = mysqli_prepare($this->con,"UPDATE platillos SET Nombre=?, Descripcion=?, Tipo=?, FechaPublicada=? WHERE Id_Platillos=?");
		$pre_->bind_param("ssssi",$this->Nombre,$this->Descripcion,$this->Tipo,$this->FechaPublicada,$this->Id_Platillos);
		$pre_->execute();
		header("location: ../nuevotema/Recetario/admiPlatillos.php");

	}



}


?>