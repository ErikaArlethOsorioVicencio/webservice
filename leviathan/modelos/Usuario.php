<?php  
namespace leviathan\modelos;
class Usuario extends Conexion
{
    public $IdUsuario;
	public $Nombre;
	public $APaterno;
	public $AMaterno;
	public $Contrasena;

	function insertar(){
		
		$pre = mysqli_prepare($this->con, "INSERT INTO usuario(Nombre, Apaterno, Amaterno, Contrasena) VALUES (?,?,?,?)");
		$pre->bind_param("ssss",$this->Nombre, $this->APaterno, $this->AMaterno, $this->Contrasena);		
		$pre->execute();
		header("location: ../nuevotema/Login/login.html");
	}
	static function busca($id)
	{
		$me = new Conexion();
		$pre = mysqli_prepare($me->con, "SELECT * FROM usuario WHERE IdUsuario= ?");
		$pre->bind_param("i",$id);
		$pre->execute();
		$res=$pre->get_result();
		return $res->fetch_object();


	}

	static function buscar($nom){
		$me = new Conexion();
		$pre = mysqli_prepare($me->con, "SELECT * FROM usuario WHERE Nombre= ?");
		$pre->bind_param("s",$nom);
		$pre->execute();
		$res=$pre->get_result();
		return $res->fetch_object();

	}
	static function all(){
		$me = new Conexion();
		$pre = mysqli_prepare($me->con, "SELECT * FROM usuario");
		$pre->execute();
		$res=$pre->get_result();
		return $res->fetch_all();

	}

	static function eliminar($quita){
		$me = new Conexion();
		$pre = mysqli_prepare($me->con, "DELETE FROM usuario WHERE IdUsuario= ?");
		$pre->bind_param("i",$quita);
		$pre->execute();
		return true;


	}
	 function actualizar(){
		$pre_ = mysqli_prepare($this->con,"UPDATE usuario SET Nombre=?, Apaterno=?,Amaterno=?, Contrasena=? WHERE IdUsuario=?");
		$pre_->bind_param("ssssi",$this->Nombre,$this->Apaterno,$this->Amaterno,$this->Contrasena,$this->IdUsuario);
		$pre_->execute();
		return true;

	}

	function login1($nom,$pass){ //Funcion que se encargará del login
		//Se hace la consulta para verificar el usuario y contraseña
		$pre = mysqli_prepare($this->con, "SELECT * FROM usuario WHERE Nombre = ? AND Contrasena = ?");
		$pre->bind_param("si", $nom, $pass);
		//Se ejecuta y optienen los resultados
		$pre->execute();
		$res=$pre->get_result();
		//Verificamos si existe algun registro con los datos
		if(mysqli_num_rows($res)==0){
			//Si no existe dato manda un error
			
			return false;
		}else{
			//Si si existe datos imprimelos
			return true;
			
		}
	}



}


?>