<?php  
namespace leviathan\modelos;
class Postres extends Conexion
{
    public $Id_Postres;
	public $Nombre;
	public $Descripcion;
	public $Tipo;
	public $FechaPublicada;

	function insertar(){
		
		$pre = mysqli_prepare($this->con, "INSERT INTO Postres(Nombre, Descripcion, Tipo, FechaPublicada) VALUES (?,?,?,?)");
		$pre->bind_param("ssss",$this->Nombre, $this->Descripcion, $this->Tipo, $this->FechaPublicada);		
		$pre->execute();
	
		header("location: ../nuevotema/RegistroPostres/registro_postre.html");
	}
	static function busca($id)
	{
		$me = new Conexion();
		$pre = mysqli_prepare($me->con, "SELECT * FROM Postres WHERE Id_Postres= ?");
		$pre->bind_param("i",$id);
		$pre->execute();
		$res=$pre->get_result();
		while ($k=mysqli_fetch_assoc($res)) {
			$z[]=$k;
		}
		return $z;


	}

	static function buscar($nom){
		$me = new Conexion();
		$pre = mysqli_prepare($me->con, "SELECT * FROM postres WHERE Nombre= ?");
		$pre->bind_param("s",$nom);
		$pre->execute();
		$res=$pre->get_result();
		return $res->fetch_object();

	}
	static function all(){
		$me = new Conexion();
		$pre = mysqli_prepare($me->con, "SELECT * FROM postres");
		$pre->execute();
		$res=$pre->get_result();
		while ($a=mysqli_fetch_assoc($res)) {
			$h[]=$a;
			
		}

		return $h;
	}

	static function eliminar($quita){
		$me = new Conexion();
		$pre = mysqli_prepare($me->con, "DELETE FROM postres WHERE Id_Postres= ?");
		$pre->bind_param("i",$quita);
		$pre->execute();

		header("location: ../nuevotema/Recetario/admiPostres.php");
		


	}
	 function actualizar(){
		$pre_ = mysqli_prepare($this->con,"UPDATE postres SET Nombre=?, Descripcion=?, Tipo=?, FechaPublicada=? WHERE Id_Postres=?");
		$pre_->bind_param("ssssi",$this->Nombre,$this->Descripcion,$this->Tipo, $this->FechaPublicada, $this->Id_Postres);
		$pre_->execute();
		header("location: ../nuevotema/Recetario/admiPostres.php");

	}



}


?>