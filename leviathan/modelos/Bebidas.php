<?php  
namespace leviathan\modelos;
class Bebidas extends Conexion
{
    public $Id_bebida;
	public $Nombre;
	public $Descripcion;
	public $Tipo;
	public $FechaPublicada;
	

	function insertar(){
		
		$pre = mysqli_prepare($this->con, "INSERT INTO bebidas(Nombre, Descripcion, Tipo, FechaPublicada) VALUES (?,?,?,?)");
		$pre->bind_param("ssss",$this->Nombre, $this->Descripcion, $this->Tipo, $this->FechaPublicada);		
		$pre->execute();
		
	
		header("location: ../nuevotema/RegistroBebidas/registro_bebida.html ");
	}
	static function busca($id)
	{
		$me = new Conexion();
		$pre = mysqli_prepare($me->con, "SELECT * FROM bebidas WHERE Id_bebida= ?");
		$pre->bind_param("i",$id);
		$pre->execute();
		$res=$pre->get_result();
		while ($k=mysqli_fetch_assoc($res)) {
			$z[]=$k;
		}
		return $z;


	}

	static function buscar($nom){
		$me = new Conexion();
		$pre = mysqli_prepare($me->con, "SELECT * FROM bebidas WHERE Nombre= ?");
		$pre->bind_param("s",$nom);
		$pre->execute();
		$res=$pre->get_result();
		return $res->fetch_object();

	}
	static function all(){
		$me = new Conexion();
		$pre = mysqli_prepare($me->con, "SELECT * FROM bebidas");
		$pre->execute();
		$res=$pre->get_result();
		while ($a=mysqli_fetch_assoc($res)) {
			$h[]=$a;
		}
		return $h;

	}

	static function eliminar($quita){
		$me = new Conexion();
		$pre = mysqli_prepare($me->con, "DELETE FROM bebidas WHERE Id_bebida= ?");
		$pre->bind_param("i",$quita);
		$pre->execute();
		header("location: ../nuevotema/Recetario/admiBebidas.php");


	}
	 function actualizar(){
		$pre_ = mysqli_prepare($this->con,"UPDATE bebidas SET Nombre=?, Descripcion=?, Tipo=?, FechaPublicada=? WHERE Id_bebida=?");
		$pre_->bind_param("ssssi",$this->Nombre,$this->Descripcion,$this->Tipo,$this->FechaPublicada, $this->Id_bebida);
		$pre_->execute();
		header("location: ../nuevotema/Recetario/admiBebidas.php");
		

	}



}


?>