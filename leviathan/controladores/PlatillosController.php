<?php  
use leviathan\modelos\Platillos;
include "modelos/Conexion.php";
include "modelos/Platillos.php";	
	class PlatillosController{	
		function __construct()
    {
        if ($_GET["action"] != "crear" && $_GET["action"] != "find" && $_GET["action"] != "Todo" && $_GET["action"] != "borrar" && $_GET["action"] != "cambiar" && $_GET["action"] != "login" ) {
            if (!isset($_SESSION["Platillos"])) {
                // Sesión no ha sido iniciada
                $_SESSION["flash"] = "No ha iniciado sesión";

                header("Location:/leviathan/index.php?controller=Usuario&action=login");
            }
        }
    }	
	function crear(){		
		if (isset($_POST)) {
				$platillos = new Platillos();
				$platillos->Nombre=$_POST["Nombre"];
				$platillos->Descripcion=$_POST["Descripcion"];
				$platillos->Tipo= $_POST["Tipo"];
				$platillos->FechaPublicada= $_POST["FechaPublicada"];
				$platillos->insertar();
			 echo json_encode(["estatus"=>"success", "platillos"=>$platillos]);
			}
		}

		public function find(){
			if(isset($_POST["id"])){
				$id=$_POST["id"];
				echo json_encode(Platillos::busca($id));
			}
		}


		public function find2(){
			if (isset($_GET["nom"])) {
				$nom=$_GET["nom"];
				echo json_encode(Platillos::buscar($nom));
			}
		}

		public function Todo(){
			Platillos::all();
			/*echo "<pre>";
				print_r(Platillos::all());
			echo json_encode("<pre/>");*/
		}

		public function borrar(){
		if (isset($_POST["id"])) {
			$quitar=$_POST["id"];
			if (Platillos::eliminar($quitar) == true) {
				echo json_encode("El dato ha sido borrado") ;
			  }

		   }
		}

		public function cambiar(){
		if (isset($_POST)) {
	    $platillos = new \leviathan\modelos\Platillos();
		$platillos->Nombre = $_POST["Nombre"];
		$platillos->Descripcion= $_POST["Descripcion"];
		$platillos->Tipo= $_POST["Tipo"];
		$platillos->FechaPublicada= $_POST["FechaPublicada"];
		$platillos->Id_Platillos = $_POST["id_"];
		$platillos->actualizar();
		echo json_encode ("Se actualizaron los datos");
		echo json_encode($platillos);

		}

		}

	}	

?>
